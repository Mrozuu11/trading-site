import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import advertisements from "./advertisements";
import filters from "./filters";
import categories from "./categories";

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  reducer: (state) => ({
    advertisements: {
      favAds: state.advertisements.favAds,
    },
  }),
});

export default new Vuex.Store({
  modules: {
    advertisements,
    filters,
    categories,
  },
  plugins: [vuexLocal.plugin],
});

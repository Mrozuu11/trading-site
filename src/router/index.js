import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import Favourites from "@/views/Favourites.vue";
import Cars from "@/views/CategoryPages/Cars/index.vue";
import AdvertDetails from "@/components/shared/AdvertDetails/index.vue";
import ErrorPage from "@/components/shared/ErrorPage/index.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/cars",
    name: "Cars",
    component: Cars,
  },
  {
    path: "/favourites",
    name: "Ulubione",
    component: Favourites,
  },
  {
    path: "/cars/:adId",
    name: "CarDetails",
    component: AdvertDetails,
  },
  { path: "*", name: 404, component: ErrorPage },
];

const router = new VueRouter({
  base: "/trading-site/",
  mode: "history",
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

export default router;

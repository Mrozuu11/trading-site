# Trading App

# The project is deployed and available to view [HERE](https://marcinmroz.me/trading-site)

The project presents a trading website where products are advertised in an array of categories. Its functionality allows the user to see numerous adverts, sort them with the use of filters, add to favourites and more. One mock category (motorisation) was created with a number of advertisements stored within the app's store, as well as a mock searchbar with a location bar.

I created this project using **Vue.JS**, employed state management with **Vuex** and **VueX Persist** as well as implemented route handling with use of **Vue Router**.
It was translated to numerous languages with **I18N** library and includes **ElementUI** components.
The main purpose was to familiarise myself with Vue.JS, its reactivity and modular build. I chose to build this project due to its high modularity and opportunity to create and employ numerous reusable components. The styling was done using **SCSS pre-processors** and helped me understand the
functionality of global stylesheet in Vue as well as **SCSS variables**. The application is responsive and works on mobile devices.

Last but not least, I used tools such as **Docker** and **NGINX** to contenerise the project and create a server with reverse proxy to serve in production mode to my [DigitalOcean droplet](https://marcinmroz.me/trading-site). Finally, the **GitLab CI/CD** process was also initialised.

## Stack:

- Vue v2
- Webpack
- VueX
- VueX Persist
- Router
- I18N
- ElementUI
- SCSS

## Demo

![landing-demo.gif](src/assets/demo/landing-demo.gif)

![car-Page-demo.gif](src/assets/demo/carPage-demo.gif)

![details-demo.gif](src/assets/demo/details-demo.gif)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve

```
